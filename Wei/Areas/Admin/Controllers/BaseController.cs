﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wei.Attributes;

namespace Wei.Areas.Admin.Controllers
{
    [Log]
    [LoginVerifyFilter()]
    public class BaseController : Controller
    {

    }
}
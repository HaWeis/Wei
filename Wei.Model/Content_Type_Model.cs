﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wei.Model
{
    public class Content_Type_Model
    {
        public int ct_id { get; set; }
        public string ct_name { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using Wei.Attributes;
using Wei.BLL;
using Wei.Model;
using Wei.Tool;

namespace Wei.Controllers
{
    [UserFilterAttribute]
    public class UserController : BaseController
    {

        public ActionResult User_Manager()
        {
            return View();
        }


        public ActionResult User_Info()
        {
            User_Model user = (User_Model)Session["User_Info"];
            //string t = new Random().NextDouble().ToString();
            //user.u_img = user.u_img + "?t=" + t;
            return View(user);
        }

        public string Edit_User(User_Model user,string user_img)
        {
            User_Model user1 = (User_Model)Session["User_Info"];
            bool not = false;
            if (user.u_phone != user1.u_phone)
            {
                not = true;
            }
            UserBLL bll = new UserBLL();
            return bll.Edit_User(user, user_img, not);
        }

        public ActionResult Tie_Zi()
        {
            return View();
        }

        public ActionResult Xiao_Xi()
        {
           
            return View();
        }



        public JsonResult Uploag_img(string u_id)
        {
            try
            {
                string savePath = string.Empty;
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var file = Request.Files[i];
                    string Extension = Path.GetExtension(file.FileName);
                    savePath = "/SiteFile/Upload/user_image/" + u_id + Extension;
                    string path = Server.MapPath("/SiteFile/Upload/user_image/" + u_id + Extension);
                    if (!System.IO.Directory.Exists(Path.GetDirectoryName(path)))
                    {
                        Directory.CreateDirectory(Path.GetDirectoryName(path));
                    }
                    path = "/SiteFile/Upload/user_image/" + u_id + ".jpg";
                    if (System.IO.File.Exists(Server.MapPath(path)))
                    {
                        System.IO.File.Delete(Server.MapPath(path));
                    }
                    path = "/SiteFile/Upload/user_image/" + u_id + ".jepg";
                    if (System.IO.File.Exists(Server.MapPath(path)))
                    {
                        System.IO.File.Delete(Server.MapPath(path));
                    }
                    path = "/SiteFile/Upload/user_image/" + u_id + ".png";
                    if (System.IO.File.Exists(Server.MapPath(path)))
                    {
                        System.IO.File.Delete(Server.MapPath(path));
                    }
                    path = "/SiteFile/Upload/user_image/" + u_id + ".gif";
                    if (System.IO.File.Exists(Server.MapPath(path)))
                    {
                        System.IO.File.Delete(Server.MapPath(path));
                    }
                    path = "/SiteFile/Upload/user_image/" + u_id + ".bmp";
                    if (System.IO.File.Exists(Server.MapPath(path)))
                    {
                        System.IO.File.Delete(Server.MapPath(path));
                    }

                    file.SaveAs(Server.MapPath(savePath));
                }
                User_Model user = (User_Model)Session["User_Info"];
                user.u_img = savePath;
                Session["User_Info"] = user;
                var json = new { code = "1", msg = "", svaePath = savePath };
                return Json(json, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.WriteFile(ex);
                var json = new { code = "0", msg = "" };
                return Json(json, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wei.Model
{
    public class Friend_Link_Model
    {
        public int fl_id { get; set; }
        public string fl_url { get; set; }
        public string fl_icon { get; set; }
        public string fl_title { get; set; }
    }
}

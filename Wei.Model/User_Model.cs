﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wei.Model
{
    public class User_Model
    {
        public int u_id { get; set; }
        public string u_name { get; set; }
        public Nullable<int> u_age { get; set; }
        public Nullable<bool> u_sex { get; set; }
        public string u_email { get; set; }
        public string u_phone { get; set; }
        public string u_password { get; set; }
        public Nullable<System.DateTime> u_create_date { get; set; }
        public Nullable<System.DateTime> u_last_update_date { get; set; }
        public int u_type { get; set; }
        public string u_img { get; set; }
        public string u_style_sign { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wei.Model
{
    public class Click_Good_Model
    {
        public int cg_id { get; set; }
        public DateTime cg_create_date { get; set; }
        public int cg_user_id { get; set; }
        public int cg_content_id { get; set; }
        public int cg_comment_id { get; set; }
    }
}

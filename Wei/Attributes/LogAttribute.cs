﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using Wei.Tool;

namespace Wei.Attributes
{
    public class LogAttribute:ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //Log.WriteFile_str("进来了===="+DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + System.Environment.NewLine);
            LogHelper.WriteLog(typeof(LogAttribute),new GetRequestInfo().GetInfo(filterContext));
        }

   

    }
}
﻿
//重写ajax方法
$.post_str = function (url, data, success, failure) {
    var r = $.extend({}, { success: success, failure: failure });
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        dataType:"text",
        error: function () {
            alert("服务器繁忙");
        }, success: function (data) {
            if (data == "code:101") {
                layer.alert("登录过期，请重新登录！", function () {
                    location.href = "/Admin/Home/Login";
                });
            } else if (data == "code:102") {
                layer.alert("登录过期，请重新登录！", function () {
                    location.href = "/";
                });
            } else {
                success(data);
            }
        }
    });
};

$.get_json = function (url, data, success, failure) {
    var r = $.extend({}, { success: success, failure: failure });
    $.ajax({
        type: "GET",
        url: url,
        data: data,
        dataType: "json",
        error: function () {
            alert("服务器繁忙");
        }, success: function (data) {
            success(data);
        }
    });
};

$.get_str = function (url, data, success, failure) {
    var r = $.extend({}, { success: success, failure: failure });
    $.ajax({
        type: "GET",
        url: url,
        data: data,
        dataType: "text",
        error: function () {
            alert("服务器繁忙");
        }, success: function (data) {
            success(data);
        }
    });
};

//$.ajaxCode = function (code) {
//    if (code == "101") {
//        layer.alert("登录过期，请重新登录！");
//        location.href = "/Admin/Home/Login";
//    } else if (code == "102") {
//        layer.alert("还没有完善信息，需要完善信息才能开始营销旅程！");
//        location.href = "/";
//    }
//};
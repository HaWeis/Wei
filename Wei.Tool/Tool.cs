﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Wei.Tool
{
    public class Tool
    {
        public static string get_conn()
        {
            string conn = System.Configuration.ConfigurationManager.ConnectionStrings["WeiEntities"].ConnectionString;
            return Password_Encrypt_ASC.Password_Encrypt_DES.get_password_ASC(conn);
        }

        public static string get_pwd(string pwd)
        {
            return Password_Encrypt_ASC.Password_Encrypt_DES.get_password_ASC(pwd);
        }

        public static string set_pwd(string pwd)
        {
            return Password_Encrypt_ASC.Password_Encrypt_DES.set_password_ASC(pwd);
        }


        ///
        /// Get请求
        /// 
        /// 
        /// 字符串
        public static string GetHttpResponse(string url, int Timeout,string contentType= "text/html")
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.ContentType = contentType + ";charset=UTF-8";
            request.UserAgent = null;
            request.Timeout = Timeout;

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream myResponseStream = response.GetResponseStream();
            StreamReader myStreamReader = new StreamReader(myResponseStream, Encoding.GetEncoding("utf-8"));
            string retString = myStreamReader.ReadToEnd();
            myStreamReader.Close();
            myResponseStream.Close();

            return retString;
        }


    }
}

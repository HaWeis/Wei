﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Wei.Attributes;

namespace Wei.Controllers
{
    [LoginVerifyFilter(true)]
    public class SiteFileController : Controller
    {
        //
        // GET: /SiteFile/
        // 微信图片转换输出
        public void wx(string Param1)
        {
            try
            {
                string filepath = MvcApplication.DirSiteFile + "wx\\" + Param1;
                if (!System.IO.File.Exists(filepath))
                {
                    filepath = System.IO.Path.GetDirectoryName(filepath) + "\\default.png";
                    if (!System.IO.File.Exists(filepath))
                    {
                        if (Regex.IsMatch(filepath, @"\d{4}.\d{2}.\d{2}"))
                        {
                            filepath = System.IO.Directory.GetParent(System.IO.Directory.GetParent(filepath).ToString()).ToString() + "\\default.png";
                            if (!System.IO.File.Exists(filepath))
                            {
                                filepath = string.Empty;
                            }
                        }
                    }
                }

                if (filepath.Length == 0)
                {
                    Response.StatusCode = 404;
                }
                else
                {
                    Response.ContentType = string.Format("image/{0}", System.IO.Path.GetExtension(filepath).Substring(1));
                    Response.WriteFile(filepath);
                }
            }
            catch (Exception)
            {
            }
        }



        public void image() //string Param1
        {
            string Param1 = Request["Param1"];
            #region 缓存输出
            DateTime dtCache;
            string time = Request.Headers["If-Modified-Since"];
            if (DateTime.TryParse(time, out dtCache))
            {
                DateTime d = DateTime.Now;
                double dd = (d - dtCache).TotalHours;
                if ((DateTime.Now - dtCache).TotalHours < 48)
                {
                    Response.StatusCode = 304;
                    Response.End();
                    return;
                }
            }
            #endregion
            try
            {
                string filepath = MvcApplication.DirSiteFile + Param1;
                if (!System.IO.File.Exists(filepath))
                {
                    filepath = System.IO.Path.GetDirectoryName(filepath) + "\\default.png";
                    if (!System.IO.File.Exists(filepath))
                    {
                        if (Regex.IsMatch(filepath, @"\d{4}.\d{2}.\d{2}"))
                        {
                            filepath = System.IO.Directory.GetParent(System.IO.Directory.GetParent(filepath).ToString()).ToString() + "\\default.png";
                            if (!System.IO.File.Exists(filepath))
                            {
                                filepath = string.Empty;
                            }
                        }
                    }
                }

                if (filepath.Length == 0)
                {
                    Response.StatusCode = 404;
                }
                else
                {
                    Response.ContentType = string.Format("image/{0}", System.IO.Path.GetExtension(filepath).Substring(1));
                    Response.WriteFile(filepath);
                }
            }
            catch (Exception)
            {

            }
            #region 设置客户端缓存
            Response.Cache.SetCacheability(HttpCacheability.Public);
            Response.Cache.SetExpires(DateTime.Now);
            Response.Cache.SetLastModified(DateTime.Now);
            #endregion

        }
        public void get(string id)
        {
            string path = MvcApplication.DirSiteFile + id;
            string extension = System.IO.Path.GetExtension(path);
            if (!System.IO.File.Exists(path))
            {
                Response.StatusCode = 404;
                return;
            }
            if (extension == ".js")
            {
                Response.ContentType = "application/x-javascript";
            }
            Response.TransmitFile(path);
        }

    }
}
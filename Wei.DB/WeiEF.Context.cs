﻿//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace Wei.DB
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class WeiEntities : DbContext
    {
        public WeiEntities()
          : base(Wei.Tool.Tool.get_conn())
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<wei_content> wei_content { get; set; }
        public virtual DbSet<wei_content_type> wei_content_type { get; set; }
        public virtual DbSet<wei_user_type> wei_user_type { get; set; }
        public virtual DbSet<wei_users> wei_users { get; set; }
        public virtual DbSet<wei_click_good> wei_click_good { get; set; }
        public virtual DbSet<wei_comment> wei_comment { get; set; }
        public virtual DbSet<wei_friend_link> wei_friend_link { get; set; }
        public virtual DbSet<wei_system> wei_system { get; set; }
        public virtual DbSet<ErrorLog> ErrorLog { get; set; }
        public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
    }
}

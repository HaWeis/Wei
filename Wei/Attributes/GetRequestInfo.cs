﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Wei.Attributes
{
    public class GetRequestInfo
    {
        public GetRequestInfo() { }
        public string GetInfo(ControllerContext filterContext)
        {
            //log4net.ILog log = log4net.LogManager.GetLogger("请求信息：");
            HttpRequestBase request = filterContext.HttpContext.Request;
            HttpResponseBase response = filterContext.HttpContext.Response;


            //Request.ApplicationPath /
            //Request.CurrentExecutionFilePath / Home / Index
            //Request.FilePath / Home / Index
            //Request.Path / Home / Index
            //Request.PhysicalApplicationPath E:\VS_Project\WorkOrder\WorkOrder.Web\
            //Request.PhysicalPath E:\VS_Project\WorkOrder\WorkOrder.Web\Home\Index
            //Request.RawUrl / Home / Index ? ID = 0
            //Request.Url http://localhost:63287/Home/Index?ID=0
            //            Request.Url.AbsolutePath / Home / Index
            //Request.Url.AbsoluteUri http://localhost:63287/Home/Index?ID=0
            //Request.Url.Scheme http
            //Request.Url.Host localhost
            //Request.Url.Port    63287
            //Request.Url.LocalPath / Home / Index
            //Request.Url.PathAndQuery / Home / Index ? ID = 0
            //Request.Url.Query ? ID = 0
            //ViewContext.RouteData.Route.GetRouteData(this.Context).Values["controller"] Home
            //ViewContext.RouteData.Route.GetRouteData(this.Context).Values["action"] Index


            StringBuilder sb = new StringBuilder();
            sb.AppendLine(System.Environment.NewLine);
            sb.AppendLine(System.Environment.NewLine);
            sb.AppendLine($"===================={DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}=========================");
            sb.AppendLine($"======================================================================");
            sb.AppendLine($"各种信息:");
            sb.AppendLine($"IP：{GetIPAddress(request)}");
            sb.AppendLine($"RawUrl：{request.RawUrl}");
            sb.AppendLine($"Url：{request.Url}");
            sb.AppendLine($"UrlReferrer：{request.UrlReferrer}");
            sb.AppendLine($"UserHostAddress：{request.UserHostAddress}");
            sb.AppendLine($"ApplicationPath：{request.ApplicationPath}");
            sb.AppendLine($"CurrentExecutionFilePath：{request.CurrentExecutionFilePath}");
            sb.AppendLine($"FilePath：{request.FilePath}");
            sb.AppendLine($"Path：{request.Path}");
            sb.AppendLine($"PhysicalApplicationPath：{request.PhysicalApplicationPath}");
            sb.AppendLine($"PhysicalPath：{request.PhysicalPath}");
            sb.AppendLine($"Url.AbsoluteUri：{request.Url.AbsoluteUri}");
            sb.AppendLine($"Url.Scheme：{request.Url.Scheme}");
            sb.AppendLine($"Url.Host：{request.Url.Host}");
            sb.AppendLine($"Url.Port：{request.Url.Port}");
            sb.AppendLine($"Url.LocalPath：{request.Url.LocalPath}");
            sb.AppendLine($"Url.PathAndQuery ：{request.Url.PathAndQuery }");
            sb.AppendLine($"Url.Query ：{request.Url.Query }");
            sb.AppendLine(System.Environment.NewLine);
            return sb.ToString();
        }

        public string GetInfo(HttpRequest request, HttpResponse response)
        {
            //Request.ApplicationPath /
            //Request.CurrentExecutionFilePath / Home / Index
            //Request.FilePath / Home / Index
            //Request.Path / Home / Index
            //Request.PhysicalApplicationPath E:\VS_Project\WorkOrder\WorkOrder.Web\
            //Request.PhysicalPath E:\VS_Project\WorkOrder\WorkOrder.Web\Home\Index
            //Request.RawUrl / Home / Index ? ID = 0
            //Request.Url http://localhost:63287/Home/Index?ID=0
            //Request.Url.AbsolutePath / Home / Index
            //Request.Url.AbsoluteUri http://localhost:63287/Home/Index?ID=0
            //Request.Url.Scheme http
            //Request.Url.Host localhost
            //Request.Url.Port    63287
            //Request.Url.LocalPath / Home / Index
            //Request.Url.PathAndQuery / Home / Index ? ID = 0
            //Request.Url.Query ? ID = 0
            //ViewContext.RouteData.Route.GetRouteData(this.Context).Values["controller"] Home
            //ViewContext.RouteData.Route.GetRouteData(this.Context).Values["action"] Index


            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"各种信息:");
            sb.AppendLine($"IP：{request.UserHostAddress}");
            sb.AppendLine($"RawUrl：{request.RawUrl}");
            sb.AppendLine($"Url：{request.Url}");
            sb.AppendLine($"UrlReferrer：{request.UrlReferrer}");
            sb.AppendLine($"UserHostAddress：{request.UserHostAddress}");
            sb.AppendLine($"ApplicationPath：{request.ApplicationPath}");
            sb.AppendLine($"CurrentExecutionFilePath：{request.CurrentExecutionFilePath}");
            sb.AppendLine($"FilePath：{request.FilePath}");
            sb.AppendLine($"Path：{request.Path}");
            sb.AppendLine($"PhysicalApplicationPath：{request.PhysicalApplicationPath}");
            sb.AppendLine($"PhysicalPath：{request.PhysicalPath}");
            sb.AppendLine($"Url.AbsoluteUri：{request.Url.AbsoluteUri}");
            sb.AppendLine($"Url.Scheme：{request.Url.Scheme}");
            sb.AppendLine($"Url.Host：{request.Url.Host}");
            sb.AppendLine($"Url.Port：{request.Url.Port}");
            sb.AppendLine($"Url.LocalPath：{request.Url.LocalPath}");
            sb.AppendLine($"Url.PathAndQuery ：{request.Url.PathAndQuery }");
            sb.AppendLine($"Url.Query ：{request.Url.Query }");
            sb.AppendLine(System.Environment.NewLine);
            return sb.ToString();
        }


        public string GetIPAddress(HttpRequestBase Request)
        {
            string userIP;
            // 如果使用代理，获取真实IP 
            if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != "")
                userIP = Request.ServerVariables["REMOTE_ADDR"];
            else
                userIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (userIP == null || userIP == "")
                userIP = Request.UserHostAddress;
            return userIP;
        }
    }
}
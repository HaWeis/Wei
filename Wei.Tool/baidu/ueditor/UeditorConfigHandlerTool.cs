﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Wei.Tool.baidu
{
    /// <summary>
    /// ueditor-1.4.3.1
    /// </summary>
    public class UeditorConfigHandlerTool : UeditorHandlerTool
    {
        public UeditorConfigHandlerTool(HttpContext context) : base(context) { }

        public override void Process()
        {
            WriteJson(UeditorConfigTool.Items);
        }
    }
}
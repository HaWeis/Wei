﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wei.Tool;

namespace Wei.Attributes
{
    public class ErrorFiterAttribute : FilterAttribute, IExceptionFilter
    {
       
        public void OnException(ExceptionContext filterContext)
        {
            LogHelper.WriteLog(typeof(LogAttribute), new GetRequestInfo().GetInfo(filterContext), filterContext.Exception);
            HttpResponseBase Response = filterContext.HttpContext.Response;
            Response.Redirect("~/500.html");
            //清除前一个异常，告诉Global全局文件的Application_Error方法不用处理了
            filterContext.HttpContext.Server.ClearError();
            return;
        }
    }
}
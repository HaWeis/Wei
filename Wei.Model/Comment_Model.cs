﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wei.Model
{
    public class Comment_Model
    {
        public int comment_id { get; set; }
        public System.DateTime comment_create_date { get; set; }
        public int comment_user_id { get; set; }
        public int comment_content_id { get; set; }
        public string comment_content { get; set; }
        public string u_name { get; set; }
        public string u_img { get; set; }
    }
}

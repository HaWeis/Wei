﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wei.DB;
using Wei.Model;

namespace Wei.BLL
{
    public class ContentBLL
    {
     



        /// <summary>
        /// 获取文章列表
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public List<Content_Model> GetContentList(int pageIndex, int pageSize,string title,int content_type, out int count)
        {
            using (WeiEntities db = new WeiEntities())
            {
                var sql = from a in db.wei_content
                          join b in db.wei_content_type on a.c_type equals b.ct_id
                          select new Content_Model
                          {
                              c_author = a.c_author,
                              c_create_date = a.c_create_date,
                              c_good = a.c_good,
                              c_id = a.c_id,
                              c_soures = a.c_soures,
                              c_read_num = a.c_read_num,
                              c_title = a.c_title,
                              c_type = a.c_type,
                              c_type_name = b.ct_name,
                              c_cover_img = a.c_cover_img,
                              c_describe = a.c_describe,
                              have_good = (db.wei_click_good.Where(c => c.cg_content_id == a.c_id)).Count()
                          };
                if (title != "")
                {
                    sql = sql.Where(c => c.c_title.Contains(title));
                }
                if (content_type != 0)
                {
                    sql = sql.Where(c => c.c_type == content_type);
                }
                count = sql.Count();

                var list = sql.OrderByDescending(t => t.c_create_date).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                return list;
            }
        }

        public string Get_User_Count()
        {
            using (WeiEntities db = new WeiEntities())
            {
                return db.wei_users.Count().ToString();
            }
        }

        public string Submit_Comment(Comment_Model model)
        {
            using (WeiEntities db = new WeiEntities())
            {
                SqlParameter[] para = new SqlParameter[] {
                        new SqlParameter("@comment_content", model.comment_content),
                        new SqlParameter("@comment_content_id", model.comment_content_id),
                        new SqlParameter("@comment_user_id", model.comment_user_id),
                    };
                string sql = "insert into wei_comment(comment_create_date,comment_content_id,comment_user_id,comment_content) values(getdate(), @comment_content_id, @comment_user_id, @comment_content)";
                return db.Database.ExecuteSqlCommand(sql, para).ToString();
            }
        }

        public Content_Model Get_Content_By_Id(int c_id,int u_id,out int have_good)
        {
            using (WeiEntities db = new WeiEntities())
            {

                var sql = from a in db.wei_content
                          join b in db.wei_content_type on a.c_type equals b.ct_id
                          where a.c_id == c_id
                          select new Content_Model
                          {
                              c_author = a.c_author,
                              c_id = a.c_id,
                              c_create_date = a.c_create_date,
                              c_content = a.c_content,
                              c_good = a.c_good,
                              c_read_num = a.c_read_num,
                              c_soures = a.c_soures,
                              c_title = a.c_title,
                              c_type = a.c_type,
                              c_describe  =a.c_describe,
                              c_cover_img = a.c_cover_img,
                              c_type_name = b.ct_name
                          };
                var model = sql.FirstOrDefault();
                have_good = 0;
                if (u_id != 0)
                {
                    have_good = db.wei_click_good.Where(c => c.cg_content_id == c_id & c.cg_user_id == u_id).Count();
                }
                if (model != null)
                {
                    return model;
                }
                return null;
            }
        }

        public List<Comment_Model> Get_Comment_List_By_Content_Id(int c_id)
        {
            using (WeiEntities db = new WeiEntities())
            {
                var sql = from a in db.wei_comment
                          join b in db.wei_users on a.comment_user_id equals b.u_id
                          where a.comment_content_id == c_id
                          orderby a.comment_create_date descending
                          select new Comment_Model
                          {
                              comment_content = a.comment_content,
                              comment_content_id = a.comment_content_id,
                              comment_create_date = a.comment_create_date,
                              comment_id = a.comment_id,
                              comment_user_id = b.u_id,
                              u_img = b.u_user_img,
                              u_name = b.u_name
                          };
                return sql.ToList();
            }
        }

        public string update_Content(Content_Model content,string hid_title, string c_describe, string c_cover_img)
        {
            using (WeiEntities db = new WeiEntities())
            {
                var model = db.wei_content.Where(c => c.c_id == content.c_id).FirstOrDefault();
                if (model == null)
                {
                    return "修改失败，文章不存在";
                }
                if (hid_title != content.c_title)
                {
                    if (model.c_title == content.c_title)
                    {
                        return "已存在同名的标题";
                    }
                }
                SqlParameter[] para = new SqlParameter[]
                {
                    new SqlParameter("@c_id",content.c_id),
                    new SqlParameter("@c_author",content.c_author),
                    new SqlParameter("@c_title",content.c_title),
                    new SqlParameter("@c_soures",content.c_soures),
                    new SqlParameter("@c_create_date",content.c_create_date),
                    new SqlParameter("@c_type",content.c_type),
                    new SqlParameter("@c_content",content.c_content),
                    new SqlParameter("@c_describe",c_describe),
                    new SqlParameter("@c_cover_img",c_cover_img)
                };
                string sql = "update wei_content set c_title=@c_title,c_type=@c_type,c_author=@c_author,c_soures=@c_soures,c_create_date=@c_create_date,c_content=@c_content,c_describe=@c_describe,c_cover_img=@c_cover_img where c_id = @c_id";
                return db.Database.ExecuteSqlCommand(sql, para).ToString();
            }
        }

        public string Del_Content(int c_id)
        {
            using (WeiEntities db = new WeiEntities())
            {
                var model = db.wei_content.Where(c => c.c_id == c_id).FirstOrDefault();
                if (model == null)
                {
                    return "删除失败，记录不存在";
                }
                SqlParameter para = new SqlParameter("@c_id",c_id);
                return db.Database.ExecuteSqlCommand("delete wei_content where c_id = @c_id", para).ToString();
            }
        }

        /// <summary>
        /// 新增文章
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public string add_Content(Content_Model content, string c_describe, string c_cover_img)
        {
            using (WeiEntities db = new WeiEntities())
            {
                var model = db.wei_content.Where(c => c.c_title == content.c_title).FirstOrDefault();
                if (model != null)
                {
                    return "已存在相同的文章标题了";
                }
                SqlParameter[] para = new SqlParameter[] {
                        new SqlParameter("@c_type", content.c_type),
                        new SqlParameter("@c_title", content.c_title),
                        new SqlParameter("@c_author", content.c_author),
                        new SqlParameter("@c_soures", content.c_soures),
                        new SqlParameter("@c_create_date", content.c_create_date),
                        new SqlParameter("@c_content", content.c_content),
                         new SqlParameter("@c_describe",c_describe),
                         new SqlParameter("@c_cover_img",c_cover_img)
                    };
                string sql = "insert into  wei_content(c_type,c_title,c_create_date,c_author,c_soures,c_content,c_describe,c_cover_img,c_good,c_read_num) values(@c_type, @c_title, @c_create_date, @c_author, @c_soures, @c_content,@c_describe,@c_cover_img, 0, 0)";
                return db.Database.ExecuteSqlCommand(sql, para).ToString();
            }
        }




        #region "文章类别相关"
        public List<Content_Type_Model> GetContentTypeList(int pageIndex, int pageSize, out int count)
        {
            using (WeiEntities db = new WeiEntities())
            {
                var sql = from a in db.wei_content_type
                          select new Content_Type_Model
                          {
                              ct_id = a.ct_id,
                              ct_name = a.ct_name
                          };
                count = sql.Count();
                return sql.OrderByDescending(t => t.ct_id).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
            }
        }


        public string AddContentType(string typeName)
        {
            using (WeiEntities db = new WeiEntities())
            {
                var model = db.wei_content_type.Where(t => t.ct_name == typeName).FirstOrDefault();
                if (model != null)
                {
                    return "新增失败，已存在同名的类别";
                }
                //wei_content_type type = new wei_content_type();
                //type.ct_name = typeName;
                //db.wei_content_type.Add(type);
                //return db.SaveChanges().ToString();
                var para = new SqlParameter("@typeName", typeName);
                return db.Database.ExecuteSqlCommand("insert into wei_content_type(ct_name) values(@typeName)", para).ToString();
            }
        }

        public string UpdateContentType(int ct_id, string ct_name)
        {
            using (WeiEntities db = new WeiEntities())
            {
                var model = db.wei_content_type.Where(t => t.ct_id == ct_id).FirstOrDefault();
                if (model == null)
                {
                    return "修改失败，记录不存在！";
                }
                SqlParameter[] para = new SqlParameter[] {
                        new SqlParameter("@ct_id", ct_id),
                        new SqlParameter("@ct_name", ct_name)
                    };
                return db.Database.ExecuteSqlCommand("update wei_content_type set ct_name = @ct_name where ct_id=@ct_id", para).ToString();
            }
        }

        public string DelContentType(int ct_id)
        {
            using (WeiEntities db = new WeiEntities())
            {
                var sql = db.wei_content.Where(c => c.c_type == ct_id).ToList();
                if (sql.Count > 0)
                {
                    return "该类别有文章在使用，请先修改再进行删除！";
                }
                var para = new SqlParameter("@ct_id", ct_id);
                return db.Database.ExecuteSqlCommand("delete wei_content_type where ct_id=@ct_id", para).ToString();
            }
        }

        public void Add_Read(string c_id)
        {
            using (WeiEntities db = new WeiEntities())
            {
                var para = new SqlParameter("@c_id", c_id);
                db.Database.ExecuteSqlCommand("update wei_content set c_read_num = c_read_num + 1 where c_id = @c_id", para);
            }
        }

        public string Add_Good(int u_id, string c_id)
        {
            using (WeiEntities db = new WeiEntities())
            {
                SqlParameter p = new SqlParameter("@c_id", c_id);
                db.Database.ExecuteSqlCommand("update wei_content set c_good = c_good + 1 where c_id = @c_id", p).ToString();
                SqlParameter[] para = new SqlParameter[]
                {
                    new SqlParameter("@u_id", u_id),
                    new SqlParameter("@c_id", c_id)
                };
                return db.Database.ExecuteSqlCommand("insert into wei_click_good(cg_create_date,cg_user_id,cg_content_id) values(getdate(),@u_id,@c_id)", para).ToString();
            }
        }
        #endregion
    }
}

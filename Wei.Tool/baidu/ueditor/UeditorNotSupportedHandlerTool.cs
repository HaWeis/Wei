﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Wei.Tool.baidu
{
    /// <summary>
    /// ueditor-1.4.3.1
    /// </summary>
    public class UeditorNotSupportedHandlerTool : UeditorHandlerTool
    {
        public UeditorNotSupportedHandlerTool(HttpContext context)
            : base(context)
        {
        }

        public override void Process()
        {
            WriteJson(new
            {
                state = "action 参数为空或者 action 不被支持。"
            });
        }
    }
}

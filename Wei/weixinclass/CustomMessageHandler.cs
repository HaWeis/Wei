﻿
using Senparc.Weixin;
using Senparc.Weixin.Context;
using Senparc.Weixin.Entities.Request;
using Senparc.Weixin.MP;
using Senparc.Weixin.MP.AdvancedAPIs;
using Senparc.Weixin.MP.Entities;
using Senparc.Weixin.MP.Entities.Request;
using Senparc.Weixin.MP.MessageHandlers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Senparc.Weixin.MP.Helpers;
using Senparc.CO2NET.Helpers;
using Senparc.Weixin.MP.Agent;
using System.Xml.Linq;
using System.Threading;
using Senparc.Weixin.Exceptions;
using System.Text;
using Wei.Tool;
using Senparc.Weixin.MP.Containers;
using Senparc.Weixin.MP.AdvancedAPIs.UserTag;
using System.Web.Script.Serialization;
using Senparc.Weixin.MP.AdvancedAPIs.OAuth;

namespace Wei.weixinclass
{
    /// <summary>
    /// 自定义MessageHandler
    /// 把MessageHandler作为基类，重写对应请求的处理方法
    /// </summary>
    public class CustomMessageHandler : MessageHandler<CustomMessageContext>
    {
        //下面的Url和Token可以用其他平台的消息，或者到www.weiweihi.com注册微信用户，将自动在“微信营销工具”下得到
        private string agentUrl = Config.SenparcWeixinSetting.AgentUrl;//这里使用了www.weiweihi.com微信自动托管平台
        private string agentToken = Config.SenparcWeixinSetting.AgentToken;//Token
        private string wiweihiKey = Config.SenparcWeixinSetting.SenparcWechatAgentKey;//WeiweihiKey专门用于对接www.Weiweihi.com平台，获取方式见：http://www.weiweihi.com/ApiDocuments/Item/25#51
        
        private string appId = Config.SenparcWeixinSetting.WeixinAppId;
        private string appSecret = Config.SenparcWeixinSetting.WeixinAppSecret;

        /// <summary>
        /// 模板消息集合（Key：checkCode，Value：OpenId）
        /// </summary>
        public static Dictionary<string, string> TemplateMessageCollection = new Dictionary<string, string>();
        public CustomMessageHandler(RequestMessageBase requestMessage)
          : base(requestMessage)
        {
        }
        public CustomMessageHandler(Stream inputStream, PostModel postModel)
          : base(inputStream, postModel)
        {

        }

        public CustomMessageHandler(Stream inputStream, PostModel postModel, int maxRecordCount = 0)
         : base(inputStream, postModel, maxRecordCount)
        {
            //这里设置仅用于测试，实际开发可以在外部更全局的地方设置，
            //比如MessageHandler<MessageContext>.GlobalWeixinContext.ExpireMinutes = 3。
            WeixinContext.ExpireMinutes = 3;

            if (!string.IsNullOrEmpty(postModel.AppId))
            {
                appId = postModel.AppId;//通过第三方开放平台发送过来的请求
            }

            //在指定条件下，不使用消息去重
            base.OmitRepeatedMessageFunc = requestMessage =>
            {
                var textRequestMessage = requestMessage as RequestMessageText;
                if (textRequestMessage != null && textRequestMessage.Content == "容错")
                {
                    return false;
                }
                return true;
            };
        }

        public override IResponseMessageBase DefaultResponseMessage(IRequestMessageBase requestMessage)
        {
            var responseMessage = base.CreateResponseMessage<ResponseMessageText>(); //ResponseMessageText也可以是News等其他类型
            responseMessage.Content = "Hello这条消息来自DefaultResponseMessage。时间："+DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            return responseMessage;
        }

        /// <summary>
        /// 处理文字请求
        /// </summary>
        /// <returns></returns>
        public override IResponseMessageBase OnTextRequest(RequestMessageText requestMessage)
        {
            //说明：实际项目中这里的逻辑可以交给Service处理具体信息，参考OnLocationRequest方法或/Service/LocationSercice.cs

            #region 历史方法

            //方法一（v0.1），此方法调用太过繁琐，已过时（但仍是所有方法的核心基础），建议使用方法二到四
            //var responseMessage =
            //    ResponseMessageBase.CreateFromRequestMessage(RequestMessage, ResponseMsgType.Text) as
            //    ResponseMessageText;

            //方法二（v0.4）
            //var responseMessage = ResponseMessageBase.CreateFromRequestMessage<ResponseMessageText>(RequestMessage);

            //方法三（v0.4），扩展方法，需要using Senparc.Weixin.MP.Helpers;
            //var responseMessage = RequestMessage.CreateResponseMessage<ResponseMessageText>();

            //方法四（v0.6+），仅适合在HandlerMessage内部使用，本质上是对方法三的封装
            //注意：下面泛型ResponseMessageText即返回给客户端的类型，可以根据自己的需要填写ResponseMessageNews等不同类型。

            #endregion
            var defaultResponseMessage = base.CreateResponseMessage<ResponseMessageText>();
            try
            {
                WeixinTrace.SendCustomLog("进来文本事件方法了", $"请求人：{requestMessage.FromUserName},请求内容：{requestMessage.Content}"+System.Environment.NewLine);
                var requestHandler =
                    requestMessage.StartHandler()
                    .Keyword("标签",()=>{
                        WeixinTrace.SendCustomLog("进来标签关键字了", $"appId:{appId},请求人：{requestMessage.FromUserName},请求内容：{requestMessage.Content}" + System.Environment.NewLine);
                        TagJson tags = UserTagApi.Get(appId);
                        if (tags.tags != null)
                        { 
                            StringBuilder sb = new StringBuilder();
                            foreach (var item in tags.tags)
                            {
                                sb.AppendLine($"标签Id：{item.id.ToString()},标签名：{item.name},此标签下粉丝数：{item.count.ToString()}");
                            }
                            defaultResponseMessage.Content = sb.ToString();
                            Log.Write_Log_For_New_File($"进来标签了：{requestMessage.FromUserName},响应的内容：{sb.ToString()}", "微信执行信息");
                        }
                        else
                        {
                            Log.Write_Log_For_New_File($"标签没有内容", "微信执行信息");
                        }
                       
                        
                        return defaultResponseMessage;
                    })
                    .Keyword("我的博客",()=> {
                        defaultResponseMessage.Content = "http://:www.chenjinwei.com";
                        return defaultResponseMessage;
                    })
                    .Keyword("我的微信页面", () => {
                         defaultResponseMessage.Content = "http://:www.chenjinwei.com/weixin/default";
                         return defaultResponseMessage;
                     })
                    .Keyword("测试",()=> {

                        StringBuilder sb = new StringBuilder();
                        sb.AppendLine("FromUserName:"+requestMessage.FromUserName);
                        sb.AppendLine("ToUserName:"+requestMessage.ToUserName);
                        sb.AppendLine("MsgId:" + requestMessage.MsgId);
                        sb.AppendLine("MsgType:" + requestMessage.MsgType);
                        sb.AppendLine("CreateTime:" + requestMessage.CreateTime);
                        sb.AppendLine("Content:" + requestMessage.Content);
                        sb.AppendLine("日期:" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                        defaultResponseMessage.Content = sb.ToString();
                        return defaultResponseMessage;
                    })
                    .Keyword("小君君", () =>
                    {
                        var uploadResult = Senparc.Weixin.MP.AdvancedAPIs.MediaApi.UploadTemporaryMedia(appId, UploadMediaFileType.image,
                                                            Server.GetMapPath("~/Images/we/1.jpg"));
                        var responseMessage = CreateResponseMessage<ResponseMessageImage>();
                        responseMessage.Image.MediaId = uploadResult.media_id;
                        return responseMessage;
                    })
                     .Keyword("大维维", () =>
                     {
                         var uploadResult = Senparc.Weixin.MP.AdvancedAPIs.MediaApi.UploadTemporaryMedia(appId, UploadMediaFileType.image,
                                                             Server.GetMapPath("~/Images/we/5.jpg"));
                         var responseMessage = CreateResponseMessage<ResponseMessageImage>();
                         responseMessage.Image.MediaId = uploadResult.media_id;
                         return responseMessage;
                     })
                     .Keyword("嘿嘿嘿", () =>
                    {
                        WeixinTrace.SendCustomLog("进来嘿嘿嘿方法了", $"请求人：{requestMessage.FromUserName},请求内容：{requestMessage.Content},请求时间{requestMessage.CreateTime}" + System.Environment.NewLine);
                        var index = new Random().Next(6,9);
                        var path = Server.GetMapPath("~/Images/we/" + index.ToString() + ".jpg");
                        var uploadResult = Senparc.Weixin.MP.AdvancedAPIs.MediaApi.UploadTemporaryMedia(appId, UploadMediaFileType.image,
                                                          path);
                        WeixinTrace.SendCustomLog("进来嘿嘿嘿方法了并上传图片成功了", $"请求人：{requestMessage.FromUserName},请求内容：{requestMessage.Content},请求时间{requestMessage.CreateTime}，media_id:{uploadResult.media_id},图片路径：{path}");
                        var responseMessage = CreateResponseMessage<ResponseMessageImage>();
                        responseMessage.Image.MediaId = uploadResult.media_id;
                        return responseMessage;
                    })
                    //                    //关键字不区分大小写，按照顺序匹配成功后将不再运行下面的逻辑
                    //                    .Keyword("约束", () =>
                    //                    {
                    //                        defaultResponseMessage.Content =
                    //                        @"您正在进行微信内置浏览器约束判断测试。您可以：
                    //<a href=""http://sdk.weixin.senparc.com/FilterTest/"">点击这里</a>进行客户端约束测试（地址：http://sdk.weixin.senparc.com/FilterTest/），如果在微信外打开将直接返回文字。
                    //或：
                    //<a href=""http://sdk.weixin.senparc.com/FilterTest/Redirect"">点击这里</a>进行客户端约束测试（地址：http://sdk.weixin.senparc.com/FilterTest/Redirect），如果在微信外打开将重定向一次URL。";
                    //                        return defaultResponseMessage;
                    //                    }).
                    //                    //匹配任一关键字
                    //                    Keywords(new[] { "托管", "代理" }, () =>
                    //                    {
                    //                    //开始用代理托管，把请求转到其他服务器上去，然后拿回结果
                    //                    //甚至也可以将所有请求在DefaultResponseMessage()中托管到外部。

                    //                    DateTime dt1 = DateTime.Now; //计时开始

                    //                    var agentXml = RequestDocument.ToString();

                    //                    #region 暂时转发到SDK线上Demo

                    //                    agentUrl = "http://sdk.weixin.senparc.com/weixin";
                    //                    //agentToken = WebConfigurationManager.AppSettings["WeixinToken"];//Token

                    //                    //修改内容，防止死循环
                    //                    var agentDoc = XDocument.Parse(agentXml);
                    //                        agentDoc.Root.Element("Content").SetValue("代理转发文字：" + requestMessage.Content);
                    //                        agentDoc.Root.Element("CreateTime").SetValue(DateTimeHelper.GetWeixinDateTime(DateTime.Now));//修改时间，防止去重
                    //                    agentDoc.Root.Element("MsgId").SetValue("123");//防止去重
                    //                    agentXml = agentDoc.ToString();

                    //                    #endregion

                    //                    var responseXml = MessageAgent.RequestXml(this, agentUrl, agentToken, agentXml);
                    //                    //获取返回的XML
                    //                    //上面的方法也可以使用扩展方法：this.RequestResponseMessage(this,agentUrl, agentToken, RequestDocument.ToString());

                    //                    /* 如果有WeiweihiKey，可以直接使用下面的这个MessageAgent.RequestWeiweihiXml()方法。
                    //                    * WeiweihiKey专门用于对接www.weiweihi.com平台，获取方式见：https://www.weiweihi.com/ApiDocuments/Item/25#51
                    //                    */
                    //                    //var responseXml = MessageAgent.RequestWeiweihiXml(weiweihiKey, RequestDocument.ToString());//获取Weiweihi返回的XML

                    //                    DateTime dt2 = DateTime.Now; //计时结束

                    //                    //转成实体。
                    //                    /* 如果要写成一行，可以直接用：
                    //                    * responseMessage = MessageAgent.RequestResponseMessage(agentUrl, agentToken, RequestDocument.ToString());
                    //                    * 或
                    //                    *
                    //                    */
                    //                        var msg = string.Format("\r\n\r\n代理过程总耗时：{0}毫秒", (dt2 - dt1).Milliseconds);
                    //                        var agentResponseMessage = responseXml.CreateResponseMessage();
                    //                        if (agentResponseMessage is ResponseMessageText)
                    //                        {
                    //                            (agentResponseMessage as ResponseMessageText).Content += msg;
                    //                        }
                    //                        else if (agentResponseMessage is ResponseMessageNews)
                    //                        {
                    //                            (agentResponseMessage as ResponseMessageNews).Articles[0].Description += msg;
                    //                        }
                    //                        return agentResponseMessage;//可能出现多种类型，直接在这里返回
                    //                })
                    //                    .Keywords(new[] { "测试", "退出" }, () =>
                    //                    {
                    //                    /*
                    //                     * 这是一个特殊的过程，此请求通常来自于微微嗨（http://www.weiweihi.com）的“盛派网络小助手”应用请求（https://www.weiweihi.com/User/App/Detail/1），
                    //                     * 用于演示微微嗨应用商店的处理过程，由于微微嗨的应用内部可以单独设置对话过期时间，所以这里通常不需要考虑对话状态，只要做最简单的响应。
                    //                     */
                    //                        if (defaultResponseMessage.Content == "测试")
                    //                        {
                    //                        //进入APP测试
                    //                        defaultResponseMessage.Content = "您已经进入【盛派网络小助手】的测试程序，请发送任意信息进行测试。发送文字【退出】退出测试对话。10分钟内无任何交互将自动退出应用对话状态。";
                    //                        }
                    //                        else
                    //                        {
                    //                        //退出APP测试
                    //                        defaultResponseMessage.Content = "您已经退出【盛派网络小助手】的测试程序。";
                    //                        }
                    //                        return defaultResponseMessage;
                    //                    })
                    //                    .Keyword("AsyncTest", () =>
                    //                    {
                    //                    //异步并发测试（提供给单元测试使用）
                    //#if NET45
                    //                    DateTime begin = DateTime.Now;
                    //                    int t1, t2, t3;
                    //                    System.Threading.ThreadPool.GetAvailableThreads(out t1, out t3);
                    //                    System.Threading.ThreadPool.GetMaxThreads(out t2, out t3);
                    //                    System.Threading.Thread.Sleep(TimeSpan.FromSeconds(4));
                    //                    DateTime end = DateTime.Now;
                    //                    var thread = System.Threading.Thread.CurrentThread;
                    //                    defaultResponseMessage.Content = string.Format("TId:{0}\tApp:{1}\tBegin:{2:mm:ss,ffff}\tEnd:{3:mm:ss,ffff}\tTPool：{4}",
                    //                            thread.ManagedThreadId,
                    //                            HttpContext.Current != null ? HttpContext.Current.ApplicationInstance.GetHashCode() : -1,
                    //                            begin,
                    //                            end,
                    //                            t2 - t1
                    //                            );
                    //#endif

                    //                    return defaultResponseMessage;
                    //                    })
                    //                    .Keyword("OPEN", () =>
                    //                    {
                    //                        var openResponseMessage = requestMessage.CreateResponseMessage<ResponseMessageNews>();
                    //                        openResponseMessage.Articles.Add(new Article()
                    //                        {
                    //                            Title = "开放平台微信授权测试",
                    //                            Description = @"点击进入Open授权页面。

                    //授权之后，您的微信所收到的消息将转发到第三方（盛派网络小助手）的服务器上，并获得对应的回复。

                    //测试完成后，您可以登陆公众号后台取消授权。",
                    //                            Url = "http://sdk.weixin.senparc.com/OpenOAuth/JumpToMpOAuth"
                    //                        });
                    //                        return openResponseMessage;
                    //                    })
                    //                    .Keyword("错误", () =>
                    //                    {
                    //                        var errorResponseMessage = requestMessage.CreateResponseMessage<ResponseMessageText>();
                    //                    //因为没有设置errorResponseMessage.Content，所以这小消息将无法正确返回。
                    //                    return errorResponseMessage;
                    //                    })
                    //                    .Keyword("容错", () =>
                    //                    {
                    //                        Thread.Sleep(4900);//故意延时1.5秒，让微信多次发送消息过来，观察返回结果
                    //                    var faultTolerantResponseMessage = requestMessage.CreateResponseMessage<ResponseMessageText>();
                    //                        faultTolerantResponseMessage.Content = string.Format("测试容错，MsgId：{0}，Ticks：{1}", requestMessage.MsgId,
                    //                            DateTime.Now.Ticks);
                    //                        return faultTolerantResponseMessage;
                    //                    })
                    //                    .Keyword("TM", () =>
                    //                    {
                    //                        var openId = requestMessage.FromUserName;
                    //                        var checkCode = Guid.NewGuid().ToString("n").Substring(0, 3);//为了防止openId泄露造成骚扰，这里启用验证码
                    //                    TemplateMessageCollection[checkCode] = openId;
                    //                        defaultResponseMessage.Content = string.Format(@"新的验证码为：{0}，请在网页上输入。网址：http://sdk.weixin.senparc.com/AsyncMethods", checkCode);
                    //                        return defaultResponseMessage;
                    //                    })
                    .Keyword("OPENID", () =>
                    {
                        var openId = requestMessage.FromUserName;//获取OpenId
                    var userInfo = Senparc.Weixin.MP.AdvancedAPIs.UserApi.Info(appId, openId, Language.zh_CN);

                        defaultResponseMessage.Content = string.Format(
                            "您的OpenID为：{0}\r\n昵称：{1}\r\n性别：{2}\r\n地区（国家/省/市）：{3}/{4}/{5}\r\n关注时间：{6}\r\n关注状态：{7}",
                            requestMessage.FromUserName, userInfo.nickname, (WeixinSex)userInfo.sex, userInfo.country, userInfo.province, userInfo.city, DateTimeHelper.GetDateTimeFromXml(userInfo.subscribe_time), userInfo.subscribe);
                        return defaultResponseMessage;
                    })
                    //.Keyword("EX", () =>
                    //{
                    //    var ex = new WeixinException("openid:" + requestMessage.FromUserName + ":这是一条测试异常信息");//回调过程在global的ConfigWeixinTraceLog()方法中
                    //defaultResponseMessage.Content = "请等待异步模板消息发送到此界面上（自动延时数秒）。\r\n当前时间：" + DateTime.Now.ToString();
                    //    return defaultResponseMessage;
                    //})
                    //.Keyword("MUTE", () => //不回复任何消息
                    //{
                    ////方案一：
                    //return new SuccessResponseMessage();

                    ////方案二：
                    //var muteResponseMessage = base.CreateResponseMessage<ResponseMessageNoResponse>();
                    //    return muteResponseMessage;

                    ////方案三：
                    //base.TextResponseMessage = "success";
                    //    return null;
                    //})
                    //.Keyword("JSSDK", () =>
                    //{
                    //    defaultResponseMessage.Content = "点击打开：http://sdk.weixin.senparc.com/WeixinJsSdk";
                    //    return defaultResponseMessage;
                    //})
                    //Default不一定要在最后一个
                    .Default(() =>
                    {
                        //var result = new StringBuilder();
                        //result.AppendFormat("您刚才发送了文字信息：{0}\r\n\r\n", requestMessage.Content);

                        //if (CurrentMessageContext.RequestMessages.Count > 1)
                        //{
                        //    result.AppendFormat("您刚才还发送了如下消息（{0}/{1}）：\r\n", CurrentMessageContext.RequestMessages.Count,
                        //        CurrentMessageContext.StorageData);
                        //    for (int i = CurrentMessageContext.RequestMessages.Count - 2; i >= 0; i--)
                        //    {
                        //        var historyMessage = CurrentMessageContext.RequestMessages[i];
                        //        result.AppendFormat("{0} 【{1}】{2}\r\n",
                        //            historyMessage.CreateTime.ToString("HH:mm:ss"),
                        //            historyMessage.MsgType.ToString(),
                        //            (historyMessage is RequestMessageText)
                        //                ? (historyMessage as RequestMessageText).Content
                        //                : "[非文字类型]"
                        //            );
                        //    }
                        //    result.AppendLine("\r\n");
                        //}

                        //result.AppendFormat("如果您在{0}分钟内连续发送消息，记录将被自动保留（当前设置：最多记录{1}条）。过期后记录将会自动清除。\r\n",
                        //    WeixinContext.ExpireMinutes, WeixinContext.MaxRecordCount);
                        //result.AppendLine("\r\n");
                        //result.AppendLine(
                        //    "您还可以发送【位置】【图片】【语音】【视频】等类型的信息（注意是这几种类型，不是这几个文字），查看不同格式的回复。\r\nSDK官方地址：http://sdk.weixin.senparc.com");

                        //defaultResponseMessage.Content = result.ToString();
                        //return defaultResponseMessage;


                        WeixinTrace.SendCustomLog("进来聊天方法了", $"请求人：{requestMessage.FromUserName},请求内容：{requestMessage.Content},请求时间{requestMessage.CreateTime}" + System.Environment.NewLine);
                        string url = "http://www.tuling123.com/openapi/api?key=306424f3584442ccbc72e21e1543f8dc";
                        if (requestMessage.Content != "")
                        {
                            url = url + "&info=" + requestMessage.Content;
                        }
                        WeixinTrace.SendCustomLog("开始访问API", $"请求人：{requestMessage.FromUserName},请求内容：{requestMessage.Content},请求时间{requestMessage.CreateTime}，请求路径：{url}" + System.Environment.NewLine);
                        string res = Tool.Tool.GetHttpResponse(url, 60000);
                        WeixinTrace.SendCustomLog("访问API结束", $"请求人：{requestMessage.FromUserName},请求内容：{requestMessage.Content},请求时间{requestMessage.CreateTime}，请求路径：{url}" + System.Environment.NewLine);
                        JavaScriptSerializer js = new JavaScriptSerializer();
                        var json = js.Deserialize<dynamic>(res);
                        
                        WeixinTrace.SendCustomLog("反序列化结果", $"请求人：{requestMessage.FromUserName},请求内容：{requestMessage.Content},请求时间{requestMessage.CreateTime}，结果：{res}" + System.Environment.NewLine);
                        StringBuilder sb = new StringBuilder();
                        //code	消息类型
                        //100000	文本类
                        //200000	链接类
                        //302000	新闻类
                        //308000	菜谱类
                        //313000	儿歌类
                        //314000	诗词类
                        int code = json["code"];
                        if (code==100000)
                        {
                            sb.Append(json["text"]);
                        }
                        else if (code==200000)
                        {
                        //"article": "因生活作风问题 北师大刑法学院院长赵秉志被免职",
                        //"source": "新浪新闻",
                        //"icon": "//k.sinaimg.cn/n/news/transform/8/w550h258/20180724/Yynx-fzrwiaz9399801.jpg/w120h90l50t10d5.jpg",
                        //"detailurl": "https://news.sina.cn/gn/2018-07-24/detail-ihftenhz8759657.d.html?vt=4&pos=8&cid=56261"
                            foreach (var item in json["list"])
                            {
                               sb.AppendLine( "作者：" + item["article"]);
                               sb.AppendLine( "来源：" + item["source"]);
                               sb.AppendLine( "链接：" + item["detailurl"]);
                            }
                        }
                        else if (code==302000)
                        {
                            sb.Append(json["text"]);
                        }
                        else if (code==308000)
                        {
                            sb.Append(json["text"]);
                        }
                        else if (code==313000)
                        {
                            sb.Append(json["text"]);
                        }
                        else if (code==314000)
                        {
                            sb.Append(json["text"]);
                        }

                        defaultResponseMessage.Content = sb.ToString();
                        return defaultResponseMessage;


                    })
                    //“一次订阅消息”接口测试
                    .Keyword("订阅", () =>
                    {
                        defaultResponseMessage.Content = "点击打开：https://sdk.weixin.senparc.com/SubscribeMsg";
                        return defaultResponseMessage;
                    })
                    //正则表达式
                    .Regex(@"^\d+#\d+$", () =>
                    {
                        defaultResponseMessage.Content = string.Format("您输入了：{0}，符合正则表达式：^\\d+#\\d+$", requestMessage.Content);
                        return defaultResponseMessage;
                    });

                return requestHandler.GetResponseMessage() as IResponseMessageBase;
            }
            catch (Exception ex)
            {
                WeixinTrace.SendCustomLog("OnTextRequest异常", $"异常信息：{ex.ToString()}" + System.Environment.NewLine);
            }
            return defaultResponseMessage;
        }


        /// <summary>
        /// 处理图片请求
        /// </summary>
        /// <param name="requestMessage"></param>
        /// <returns></returns>
        public override IResponseMessageBase OnImageRequest(RequestMessageImage requestMessage)
        {
            //一隔一返回News或Image格式
            //if (base.WeixinContext.GetMessageContext(requestMessage).RequestMessages.Count() % 2 == 0)
            //{
            //    var responseMessage = CreateResponseMessage<ResponseMessageNews>();

            //    responseMessage.Articles.Add(new Article()
            //    {
            //        Title = "您刚才发送了图片信息",
            //        Description = "您发送的图片将会显示在边上",
            //        PicUrl = requestMessage.PicUrl,
            //        Url = "http://sdk.weixin.senparc.com"
            //    });
            //    responseMessage.Articles.Add(new Article()
            //    {
            //        Title = "第二条",
            //        Description = "第二条带连接的内容",
            //        PicUrl = requestMessage.PicUrl,
            //        Url = "http://sdk.weixin.senparc.com"
            //    });

            //    return responseMessage;
            //}
            //else
            //{
            //    var responseMessage = CreateResponseMessage<ResponseMessageImage>();
            //    Log.Write_Log_For_New_File("进来了3：MediaId="+ requestMessage.MediaId, "微信执行信息");
            //    responseMessage.Image.MediaId = requestMessage.MediaId;
            //    return responseMessage;
            //}
            var responseMessage = CreateResponseMessage<ResponseMessageImage>();
            Log.Write_Log_For_New_File("进来了3：MediaId=" + requestMessage.MediaId, "微信执行信息");
            responseMessage.Image.MediaId = requestMessage.MediaId;
            return responseMessage;
        }

        /// <summary>
        /// 处理语音请求
        /// </summary>
        /// <param name="requestMessage"></param>
        /// <returns></returns>
        public override IResponseMessageBase OnVoiceRequest(RequestMessageVoice requestMessage)
        {
            var responseMessage = CreateResponseMessage<ResponseMessageMusic>();
            //上传缩略图
            //var accessToken = Containers.AccessTokenContainer.TryGetAccessToken(appId, appSecret);
            var uploadResult = Senparc.Weixin.MP.AdvancedAPIs.MediaApi.UploadTemporaryMedia(appId, UploadMediaFileType.image,
                                                         Server.GetMapPath("~/Images/Logo.jpg"));

            //设置音乐信息
            responseMessage.Music.Title = "天籁之音";
            responseMessage.Music.Description = "播放您上传的语音";
            responseMessage.Music.MusicUrl = "http://sdk.weixin.senparc.com/Media/GetVoice?mediaId=" + requestMessage.MediaId;
            responseMessage.Music.HQMusicUrl = "http://sdk.weixin.senparc.com/Media/GetVoice?mediaId=" + requestMessage.MediaId;
            responseMessage.Music.ThumbMediaId = uploadResult.media_id;

            //推送一条客服消息
            try
            {
                CustomApi.SendText(appId, WeixinOpenId, "本次上传的音频MediaId：" + requestMessage.MediaId);

            }
            catch
            {
            }

            return responseMessage;
        }

        /// <summary>
        /// 处理视频请求
        /// </summary>
        /// <param name="requestMessage"></param>
        /// <returns></returns>
        public override IResponseMessageBase OnVideoRequest(RequestMessageVideo requestMessage)
        {
            var responseMessage = CreateResponseMessage<ResponseMessageText>();
            responseMessage.Content = "您发送了一条视频信息，ID：" + requestMessage.MediaId;

            #region 上传素材并推送到客户端

            Task.Factory.StartNew(async () =>
            {
                //上传素材
                var dir = Server.GetMapPath("~/App_Data/TempVideo/");
                var file = await MediaApi.GetAsync(appId, requestMessage.MediaId, dir);
                var uploadResult = await MediaApi.UploadTemporaryMediaAsync(appId, UploadMediaFileType.video, file, 50000);
                await CustomApi.SendVideoAsync(appId, base.WeixinOpenId, uploadResult.media_id, "这是您刚才发送的视频", "这是一条视频消息");
            }).ContinueWith(async task =>
            {
                if (task.Exception != null)
                {
                    WeixinTrace.Log("OnVideoRequest()储存Video过程发生错误：", task.Exception.Message);

                    var msg = string.Format("上传素材出错：{0}\r\n{1}",
                               task.Exception.Message,
                               task.Exception.InnerException != null
                                   ? task.Exception.InnerException.Message
                                   : null);
                    await CustomApi.SendTextAsync(appId, base.WeixinOpenId, msg);
                }
            });

            #endregion

            return responseMessage;
        }

        

        /// <summary>
        /// 处理事件请求（这个方法一般不用重写，这里仅作为示例出现。除非需要在判断具体Event类型以外对Event信息进行统一操作
        /// </summary>
        /// <param name="requestMessage"></param>
        /// <returns></returns>
        public override IResponseMessageBase OnEventRequest(IRequestMessageEventBase requestMessage)
        {

            //OAuthUserInfo userInfo = null;
            //try
            //{
            //    //已关注，可以得到详细信息
            //    userInfo = OAuthApi.GetUserInfo(result.access_token, result.openid);

            //    if (!string.IsNullOrEmpty(returnUrl))
            //    {
            //        return Redirect(returnUrl);
            //    }


            //    ViewData["ByBase"] = true;
            //    return View("UserInfoCallback", userInfo);
            //}
            //catch (ErrorJsonResultException ex)
            //{
            //    //未关注，只能授权，无法得到详细信息
            //    //这里的 ex.JsonResult 可能为："{\"errcode\":40003,\"errmsg\":\"invalid openid\"}"
            //    return Content("用户已授权，授权Token：" + result);
            //}

            if (requestMessage.Event== Event.subscribe)
            {
                var defaultResponseMessage = base.CreateResponseMessage<ResponseMessageText>();
                defaultResponseMessage.Content = "你好呀，我是聊天机器人，你可以随意跟我聊天解闷哟~";
                return defaultResponseMessage;
            }
            else
            {
                var eventResponseMessage = base.OnEventRequest(requestMessage);//对于Event下属分类的重写方法，见：CustomerMessageHandler_Events.cs
                //TODO: 对Event信息进行统一操作
                return eventResponseMessage;
            }






        }


    }


}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wei.Model
{
    public class System_Model
    {
        public int sy_id { get; set; }
        public string s_name { get; set; }
        public string s_qq { get; set; }
        public string s_wechar { get; set; }
        public string s_img { get; set; }
        public string s_smpt { get; set; }
        public string s_email { get; set; }
        public string s_code { get; set; }
        public string sy_show_comment { get; set; }
        public string sy_show_friend_link { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Wei.Attributes
{
    public class LoginVerifyFilterAttribute:ActionFilterAttribute
    {
        private bool white = false;
        public LoginVerifyFilterAttribute(bool white = false)
        {
            this.white = white;
        }
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (white) return;
            HttpResponseBase Response = filterContext.HttpContext.Response;
            HttpSessionStateBase Session = filterContext.HttpContext.Session;
            if (Session == null || Session["Admin_User_Info"] == null)
            {
                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    //filterContext.HttpContext.Response.StatusCode = 101;//这个可以指定为其他的
                    //filterContext.Result = new JsonResult
                    //{
                    //    //Data = new
                    //    //{
                    //    //    ErrorMessage = "您长时间没有操作，请重新登录！"
                    //    //}, //这样使用，最终的结果判断时，xhr.responseText为"{ErrorMessage:"您长时间没有操作，请重新登录！"}",还需要Json转化一下
                    //    Data = "您长时间没有操作，请重新登录！",
                    //    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    //};
                    //filterContext.HttpContext.Response.End();

                    //Response.Write("code:101");
                    if (filterContext.HttpContext.Request.RequestContext.RouteData.DataTokens["Area"].ToString() == "Admin")
                    {
                        //Response.Write("<script type=\"text/javascript\">layer.alert('登录过期,请重新登录！');location.href = '/Admin/Home/Login';</script>");
                        Response.Write("code:101");
                    }
                    else
                    {
                        //Response.Write("<script type=\"text/javascript\">layer.alert('登录过期,请重新登录！');location.href = '/';</script>");
                        Response.Write("code:102");
                    }
                   
                    filterContext.Result = new EmptyResult();
                    return;
                }
                else
                {
                    Response.Redirect("/Admin/Home/Login");
                    filterContext.Result = new EmptyResult();
                    return;
                }
              
            }
        }
    }
}

--用户类别
create table wei_user_type(
 ut_id int identity(1,1) not null,
 ut_name varchar(20)
)

--用户表
create table wei_users(
 u_id int identity(1,1) not null,
 u_name varchar(30) not null,
 u_age int,
 u_sex bit,
 u_email varchar(100) not null,
 u_phone varchar(20),
 u_password varchar(120) not null,
 u_create_date datetime not null,
 u_last_update_date datetime,
 u_type int not null			--用户类型
)

--文章类型
create table wei_content_type(
ct_id int identity(1,1) not null,
ct_name varchar(20) not null,
)

--文章表
create table wei_content(
c_id int identity(1,1) not null,
c_type int not null,--文字类别
c_content nvarchar(max), 
c_title varchar(80),
c_soures varchar(80),--文章来源
c_author varchar(20),--文章作者
c_create_date datetime,
c_read_num int, --阅读量
c_good int, --点赞量
)
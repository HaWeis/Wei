﻿
using System;
using System.Collections;
using System.Web;
using Wei.Tool;
using Wei.Tool.baidu;

namespace Wei.Areas.Admin.Controllers
{
    // GET: Admin/ueditor
    public class ueditorController : BaseController
    {
        //
        // GET: /ss/ueditor/

        public void config()
        {
            HttpContext context = HttpContext.ApplicationInstance.Context;
            UeditorHandlerTool action = null;
            switch (context.Request["action"])
            {
                case "config":
                    action = new UeditorConfigHandlerTool(context);
                    break;
                case "uploadimage":
                    action = new UeditorUploadHandlerTool(context, new UeditorUploadConfigTool()
                    {
                        AllowExtensions = UeditorConfigTool.GetStringList("imageAllowFiles"),
                        PathFormat = UeditorConfigTool.GetString("imagePathFormat"),
                        SizeLimit = UeditorConfigTool.GetInt("imageMaxSize"),
                        UploadFieldName = UeditorConfigTool.GetString("imageFieldName")
                    });
                    break;
                case "uploadscrawl":
                    action = new UeditorUploadHandlerTool(context, new UeditorUploadConfigTool()
                    {
                        AllowExtensions = new string[] { ".png" },
                        PathFormat = UeditorConfigTool.GetString("scrawlPathFormat"),
                        SizeLimit = UeditorConfigTool.GetInt("scrawlMaxSize"),
                        UploadFieldName = UeditorConfigTool.GetString("scrawlFieldName"),
                        Base64 = true,
                        Base64Filename = "scrawl.png"
                    });
                    break;
                case "uploadvideo":
                    action = new UeditorUploadHandlerTool(context, new UeditorUploadConfigTool()
                    {
                        AllowExtensions = UeditorConfigTool.GetStringList("videoAllowFiles"),
                        PathFormat = UeditorConfigTool.GetString("videoPathFormat"),
                        SizeLimit = UeditorConfigTool.GetInt("videoMaxSize"),
                        UploadFieldName = UeditorConfigTool.GetString("videoFieldName")
                    });
                    break;
                case "uploadfile":
                    action = new UeditorUploadHandlerTool(context, new UeditorUploadConfigTool()
                    {
                        AllowExtensions = UeditorConfigTool.GetStringList("fileAllowFiles"),
                        PathFormat = UeditorConfigTool.GetString("filePathFormat"),
                        SizeLimit = UeditorConfigTool.GetInt("fileMaxSize"),
                        UploadFieldName = UeditorConfigTool.GetString("fileFieldName")
                    });
                    break;
                case "listimage":
                    action = new UeditorListFileHandlerTool(context, UeditorConfigTool.GetString("imageManagerListPath"), UeditorConfigTool.GetStringList("imageManagerAllowFiles"));
                    break;
                case "listfile":
                    action = new UeditorListFileHandlerTool(context, UeditorConfigTool.GetString("fileManagerListPath"), UeditorConfigTool.GetStringList("fileManagerAllowFiles"));
                    break;
                case "catchimage":
                    action = new UeditorCrawlerHandlerTool(context);
                    break;
                default:
                    action = new UeditorNotSupportedHandlerTool(context);
                    break;
            }
            action.Process();
        }

        public string imageUp()
        {
            Log.WriteFile_str("进来了imageUp");
            Response.ContentEncoding = System.Text.Encoding.UTF8;
            //上传配置
            string pathbase = string.Format("ueditor\\image\\{0:yyyyMMdd}\\", DateTime.Now);                                                          //保存路径
            int size = 10;                     //文件大小限制,单位mb                                                                                   //文件大小限制，单位KB
            string[] filetype = { ".gif", ".png", ".jpg", ".jpeg", ".bmp" };                    //文件允许格式

            string callback = Request["callback"];
            string editorId = Request["editorid"];

            //上传图片
            Hashtable info;
            UploaderTool up = new UploaderTool();
            HttpContext context = HttpContext.ApplicationInstance.Context;
            info = up.upFile(context, pathbase, filetype, size); //获取上传状态
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(info);

            Response.ContentType = "text/html";
            if (callback != null)
            {
                return (String.Format("<script>{0}(JSON.parse(\"{1}\"));</script>", callback, json));
            }
            else
            {
                return json;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wei.Model
{
    public class Content_Model
    {
        public int c_id { get; set; }
        public int c_type { get; set; }
        public string c_content { get; set; }
        public string c_title { get; set; }
        public string c_soures { get; set; }
        public string c_author { get; set; }
        public string c_type_name { get; set; }
        public Nullable<System.DateTime> c_create_date { get; set; }
        public Nullable<int> c_read_num { get; set; }
        public Nullable<int> c_good { get; set; }
        public string c_cover_img { get; set; }
        public string c_describe { get; set; }
        public int have_good { get; set; }
    }
}

﻿using System.Web.Mvc;
using Wei.BLL;
using Wei.Model;

namespace Wei.Controllers
{
    public class ContentController : BaseController
    {
        public string Add_Good(string c_id="0")
        {
            if (Session["User_Info"] != null & c_id !="0")
            {
                ContentBLL bll = new ContentBLL();
                User_Model model = (User_Model)Session["User_Info"];
                return bll.Add_Good(model.u_id, c_id);
            }
            return "";
        }

        [HttpPost]
        public string Submit_Comment(Comment_Model model)
        {
            if (model == null)
            {
                return "";
            }
            if (model.comment_content_id == 0)
            {
                return "";
            }
            if (model.comment_user_id == 0)
            {
                return "";
            }
            if (model.comment_content == "")
            {
                return "";
            }
            ContentBLL bll = new ContentBLL();
            return bll.Submit_Comment(model);
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using Wei.Attributes;
using Wei.BLL;
using Wei.Model;
using Wei.Tool;

namespace Wei.Areas.Admin.Controllers
{
    public class HomeController : BaseController
    {
        // GET: Admin/Home
        public ActionResult Index()
        {
          
            return View();
        }

        [LoginVerifyFilter(true)]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [LoginVerifyFilter(true)]
        public string Login(string email, string password)
        {
            try
            {
                UserBLL bll = new UserBLL();
                User_Model user = bll.Login(email, password,1);
                if (user != null)
                {
                    Session["Admin_User_Info"] = user;
                    return "ok";
                }
                return "用户名或密码错误";
            }
            catch (Exception ex)
            {
                return "登录失败，出现异常" + ex.ToString();
            }
        }
        [LoginVerifyFilter(true)]
        public ActionResult LoginOut()
        {
            Session["User_Info"] = null;
            return Redirect("/Home/Login");
        }

        public ActionResult Index_Img()
        {
            UserBLL bll = new UserBLL();
            System_Model model = bll.Get_Index_Img();
            string img = "";
            if (model != null)
            {
                if (model.s_img.Contains(";"))
                    img = model.s_img.Substring(0, model.s_img.Length - 1);
            }
            ViewBag.img_arr = img.Split(';');
            return View();
        }

        public JsonResult Uploag_img()
        {
            try
            {
                string svaePath = string.Empty;
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var file = Request.Files[i];
                    string Extension = Path.GetExtension(file.FileName);
                    //svaePath = string.Format("/SiteFile/Upload/Index_Img/index_{0}{1}",i.ToString(), Extension);
                    svaePath = string.Format("/SiteFile/Upload/Index_Img/{0}", file.FileName);
                    string path = Server.MapPath(svaePath);
                    if (!System.IO.Directory.Exists(path))
                    {
                        Directory.CreateDirectory(Path.GetDirectoryName(path));
                    }
                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }
                    file.SaveAs(path);
                }
                var json = new { code = "0", msg = "", svaePath = svaePath};
                return Json(json, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.WriteFile(ex);
                var json = new { code = "0", msg = "" };
                return Json(json, JsonRequestBehavior.AllowGet);
            }
        }

        public string Update_Index_Img(string svaePath)
        {
            UserBLL bll = new UserBLL();
            return bll.Update_Index_Img(svaePath);
        }


        public ActionResult Site_Setting()
        {

            return View();
        }

        public JsonResult Friend_Link_List(string pageIndex = "1", string pageSize = "5",string fl_title = "")
        {
            try
            {
                int count = 0;
                UserBLL bll = new UserBLL();
                List<Friend_Link_Model> list = bll.Friend_Link_List(int.Parse(pageIndex), int.Parse(pageSize), fl_title, out count);
                var data = new { code = "0", msg = "友情链接列表", count = count.ToString(), data = list };
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.WriteFile(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }


        public string Add_Friend_Link(string fl_title,string fl_url)
        {
            UserBLL bll = new UserBLL();
            return bll.Add_Friend_Link(fl_title, fl_url);
        }

        public string Update_Friend_Link(string title, string url, string id = "0")
        {
            UserBLL bll = new UserBLL();
            return bll.Update_Friend_Link(int.Parse(id), title, url);
        }

        public string Update_System_Data(string sy_show_comment, string sy_show_friend_link)
        {
            UserBLL bll = new UserBLL();
            Session["sy_show_comment"] = sy_show_comment;
            Session["sy_show_friend_link"] = sy_show_friend_link;
            return bll.Update_System_Data(sy_show_comment, sy_show_friend_link);
        }

        public string Delete_Friend_Link(string id = "0")
        {
            UserBLL bll = new UserBLL();
            return bll.Delete_Friend_Link(int.Parse(id));
        }


        public ActionResult User_List()
        {
            return View();
        }

        public JsonResult Get_User_List(string pageIndex = "1", string pageSize = "5", string nickName = "")
        {
            try
            {
                int count = 0;
                UserBLL bll = new UserBLL();
                List<User_Model> list = bll.GetUserList(int.Parse(pageIndex), int.Parse(pageSize), nickName, out count);
                var data = new { code = "0", msg = "获取用户列表", count = count.ToString(), data = list };
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.WriteFile(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public string Del_User(string u_id)
        {
            UserBLL bll = new UserBLL();
            string a = bll.Del_User(int.Parse(u_id));
            return a.ToString();
        }

        public string Edit_User(string u_id,string u_name,string u_phone,string u_email)
        {
            UserBLL bll = new UserBLL();
            string a = bll.Edit_User(int.Parse(u_id),u_name,u_phone,u_email);
            return a.ToString();
        }

        public string Chong_Zhi(string id)
        {
            UserBLL bll = new UserBLL();
            string pwd = "";
            string a = bll.Chong_Zhi(int.Parse(id),ref pwd);
            return pwd;
        }

        public JsonResult Get_System_Data(string pageIndex = "1", string pageSize = "5", string fl_title = "")
        {
            try
            {
                int count = 0;
                UserBLL bll = new UserBLL();
                List<System_Model> list = bll.Get_System_Data(int.Parse(pageIndex), int.Parse(pageSize), fl_title, out count);
                var data = new { code = "0", msg = "系统设置", count = count.ToString(), data = list };
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.WriteFile(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }






    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Wei
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            //设置默认启动后台页面
            //routes.MapRoute(
            //    "Admin",
            //    "Admin/{controller}/{action}/{id}",
            //    new { controller = "Home", action = "Index", id = UrlParameter.Optional }, //这里要和Admin下的默认控制器和action一样  
            //    new[] { "Wei.Areas.Admin.Controllers" }// 这个是设置默认页控制器所在命名空间  
            //).DataTokens.Add("area", "Admin"); //（Admin就是Areas下一级的名称）

            //设置默认启动前台页面
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
                ,namespaces: new string[] { "Wei.Controllers" }
            );



        }
    }
}

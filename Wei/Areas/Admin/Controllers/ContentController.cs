﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using Wei.BLL;
using Wei.Model;
using Wei.Tool;

namespace Wei.Areas.Admin.Controllers
{
    public class ContentController : BaseController
    {
        // GET: Admin/Content
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ContentManager()
        {
            return View();
        }

        public ActionResult AddContent(string c_id)
        {
            if (!string.IsNullOrEmpty(c_id))
            {
                ContentBLL bll = new ContentBLL();
                int a = 0;
                Content_Model content = bll.Get_Content_By_Id(int.Parse(c_id),0,out a);
                if (content != null)
                {
                    ViewBag.c_title = content.c_title;
                    ViewBag.c_author = content.c_author;
                    if (content.c_content != "")
                    {
                        ViewBag.have = "yes";
                    }
                    else
                    {
                        ViewBag.have = "no";
                    }
                    ViewBag.c_content = JsonConvert.SerializeObject(content.c_content);
                    ViewBag.c_id = content.c_id;
                    ViewBag.c_soures = content.c_soures;
                    ViewBag.c_type = content.c_type;
                    ViewBag.c_create_date = string.Format("{0:yyyy-MM-dd}", content.c_create_date);
                    ViewBag.c_cover_img = content.c_cover_img;
                    ViewBag.c_describe = content.c_describe;
                    ViewBag.Edit = "编辑文章";
                }
            }
            else
            {
                ViewBag.have = "no";
                ViewBag.c_content = "no";
                ViewBag.Edit = "新增文章";
                ViewBag.c_cover_img = string.Format("/images/default{0}.jpg", new Random().Next(1, 3).ToString());
            }
            return View();
        }


        [HttpPost]
        [ValidateInput(false)]//防止危险字符提交
        public string add_Content(Content_Model content, string hid_title,string c_describe,string c_cover_img)
        {
            try
            {
                string res = "";
                if (content.c_author == null)
                {
                    content.c_author = "";
                }
                //如果c_id不为空表示修改
                if (!string.IsNullOrEmpty(content.c_id.ToString()) && content.c_id != 0)
                {
                    ContentBLL bll = new ContentBLL();
                    res = bll.update_Content(content, hid_title,c_describe,c_cover_img);
                }
                else
                {
                    ContentBLL bll = new ContentBLL();
                    res = bll.add_Content(content,c_describe,c_cover_img);
                }
                return res;
            }
            catch (Exception ex)
            {
                Log.WriteFile(ex);
                return ex.ToString();
            }
        }

        public string Del_Content(string c_id)
        {
            try
            {
                ContentBLL bll = new ContentBLL();
                return bll.Del_Content(int.Parse(c_id));
            }
            catch (Exception ex)
            {
                Log.WriteFile(ex);
                return ex.ToString();
            }
        }

        public JsonResult ContentList(string pageIndex = "1", string pageSize = "10", string s_title = "",string s_type="0")
        {
            try
            {
                int count = 0;
                ContentBLL bll = new ContentBLL();
                List<Content_Model> list = bll.GetContentList(int.Parse(pageIndex), int.Parse(pageSize), s_title,int.Parse(s_type), out count);
                var data = new { code = "0", msg = "获取文章列表", count = count.ToString(), data = list };
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.WriteFile(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ContentType()
        {
            return View();
        }

        /// <summary>
        /// 获取文章类型列表
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public JsonResult ContentTypeList(string pageIndex = "1", string pageSize = "5")
        {
            try
            {
                int count = 0;
                ContentBLL bll = new ContentBLL();
                List<Content_Type_Model> list = bll.GetContentTypeList(int.Parse(pageIndex), int.Parse(pageSize), out count);
                var data = new { code = "0", msg = "获取文章类别", count = count.ToString(), data = list };
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.WriteFile(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 新增文章类型
        /// </summary>
        /// <param name="typeName"></param>
        /// <returns></returns>
        public string AddContentType(string typeName)
        {
            try
            {
                ContentBLL bll = new ContentBLL();
                return bll.AddContentType(typeName.Trim());
            }
            catch (Exception ex)
            {
                Log.WriteFile(ex);
                return ex.ToString();
            }
        }
        public string DelContentType(string id)
        {
            try
            {
                ContentBLL bll = new ContentBLL();
                return bll.DelContentType(int.Parse(id));
            }
            catch (Exception ex)
            {
                Log.WriteFile(ex);
                return ex.ToString();
            }
        }
        public string UpdateContentType(string id, string name)
        {
            try
            {
                ContentBLL bll = new ContentBLL();
                return bll.UpdateContentType(int.Parse(id), name.Trim());
            }
            catch (Exception ex)
            {
                Log.WriteFile(ex);
                return ex.ToString();
            }
        }


        public JsonResult Uploag_img(string c_id)
        {
            try
            {
                string svaePath = string.Empty;
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var file = Request.Files[i];
                    string Extension = Path.GetExtension(file.FileName);
                    svaePath = "/SiteFile/Upload/content_image/" + c_id + Extension;
                    string path = Server.MapPath("/SiteFile/Upload/content_image/" + c_id + Extension);
                    if (!System.IO.Directory.Exists(Path.GetDirectoryName(path)))
                    {
                        Directory.CreateDirectory(Path.GetDirectoryName(path));
                    }

                    path = "/SiteFile/Upload/content_image/" + c_id + ".jpg";
                    if (System.IO.File.Exists(Server.MapPath(path)))
                    {
                        System.IO.File.Delete(Server.MapPath(path));
                    }
                    path = "/SiteFile/Upload/content_image/" + c_id + ".jepg";
                    if (System.IO.File.Exists(Server.MapPath(path)))
                    {
                        System.IO.File.Delete(Server.MapPath(path));
                    }
                    path = "/SiteFile/Upload/content_image/" + c_id + ".png";
                    if (System.IO.File.Exists(Server.MapPath(path)))
                    {
                        System.IO.File.Delete(Server.MapPath(path));
                    }
                    path = "/SiteFile/Upload/content_image/" + c_id + ".gif";
                    if (System.IO.File.Exists(Server.MapPath(path)))
                    {
                        System.IO.File.Delete(Server.MapPath(path));
                    }
                    path = "/SiteFile/Upload/content_image/" + c_id + ".bmp";
                    if (System.IO.File.Exists(Server.MapPath(path)))
                    {
                        System.IO.File.Delete(Server.MapPath(path));
                    }

                    file.SaveAs(Server.MapPath(svaePath));
                }
                var json = new { code = "1", msg = "", svaePath= svaePath };
                return Json(json, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.WriteFile(ex);
                var json = new { code = "0", msg = "" };
                return Json(json, JsonRequestBehavior.AllowGet);
            }
        }




    }
}
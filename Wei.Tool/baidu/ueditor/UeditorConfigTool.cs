﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace Wei.Tool.baidu
{
    /// <summary>
    /// ueditor-1.4.3.1
    /// </summary>
    public class UeditorConfigTool
    {
        public static string UeditorConfigPath;
        private static bool noCache = true;
        private static JObject BuildItems()
        {
            //var json = File.ReadAllText(HttpContext.Current.Server.MapPath("config.json"));
            //return JObject.Parse(json);
            string json = File.ReadAllText(UeditorConfigPath);
            return JObject.Parse(json);
        }

        public static JObject Items
        {
            get
            {
                if (noCache || _Items == null)
                {
                    try
                    {
                    _Items = BuildItems();
                    }
                    catch (Exception)
                    {
                        
                        throw;
                    }
                }
                return _Items;
            }
        }
        private static JObject _Items;


        public static T GetValue<T>(string key)
        {
            return Items[key].Value<T>();
        }

        public static String[] GetStringList(string key)
        {
            return Items[key].Select(x => x.Value<String>()).ToArray();
        }

        public static String GetString(string key)
        {
            return GetValue<String>(key);
        }

        public static int GetInt(string key)
        {
            return GetValue<int>(key);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Wei.Attributes
{
    public class UserFilterAttribute : ActionFilterAttribute
    {
        private bool white = false;
        public UserFilterAttribute(bool white = false)
        {
            this.white = white;
        }
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (white) return;
            HttpResponseBase Response = filterContext.HttpContext.Response;
            HttpSessionStateBase Session = filterContext.HttpContext.Session;
            if (Session == null || Session["User_Info"] == null)
            {
                Response.Redirect("/Home/Index");
                filterContext.Result = new EmptyResult();
                return;
            }
        }

     
    }
}